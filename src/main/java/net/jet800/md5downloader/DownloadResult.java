package net.jet800.md5downloader;

import java.io.File;
import java.net.URL;

public class DownloadResult {

  final URL source;
  final File localFile;
  final String expectedMD5;
  final String actualMD5;

  public DownloadResult(URL source, File localFile, String expectedMD5, String actualMD5) {
    this.source = source;
    this.localFile = localFile;
    this.expectedMD5 = expectedMD5;
    this.actualMD5 = actualMD5;
  }

  @Override
  public String toString() {
    return "Downloaded file:" + localFile.getName() +
      ", expected md5: " + expectedMD5 +
      ", received md5: " + actualMD5;
  }
}
