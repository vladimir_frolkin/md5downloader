package net.jet800.md5downloader;

import org.apache.commons.io.FilenameUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Md5Downloader {

  final ExecutorService executorService = Executors.newCachedThreadPool();

  public static void main(String[] args) {
    if (args.length == 1) {
      try {
        URL source = new URL(args[0]);
        Md5Downloader md5Downloader = new Md5Downloader();
        List<DownloadResult> results = md5Downloader.download(source);
        results.forEach(System.out::println);
      } catch (MalformedURLException e) {
        System.err.println("Invalid source URL: " + e);
      } catch (IOException e) {
        System.err.println("An I/O error occurred while downloading file list: " + e);
      }
    } else {
      System.err.println("Invalid parameter count. Please specify only URL of file list to download.");
    }
  }

  /**
   * Gets a file with comma-separated list of links to download and their's md5 checksums, downloads such file and somputes actual md5 checksums.
   * Files are download into working directory.
   * @param source location of file list
   * @return download results
   * @throws IOException if file list could not be downloaded
   */
  public List<DownloadResult> download(URL source) throws IOException {
    List<Future<DownloadResult>> futures = new LinkedList<>();
    try (BufferedReader reader = new BufferedReader(new InputStreamReader(source.openStream()))) {
      String line;
      while ((line = reader.readLine()) != null) {
        String[] s = line.split(",");
        if (s.length == 2) {
          try {
            URL url = new URL(s[0]);
            futures.add(executorService.submit(new DownloadTask(url, new File(FilenameUtils.getName(url.getPath())), s[1])));
          } catch (MalformedURLException e) {
            System.err.println("Malformed input: " + e);
          }
        } else {
          System.err.println("Malformed input: " + line);
        }
      }
    }
    List<DownloadResult> results = new ArrayList<>(futures.size());
    for (Future<DownloadResult> f : futures) {
      try {
        results.add(f.get());
      } catch (InterruptedException | ExecutionException e) {
        System.err.println("Failed to process file: " + e);
      }
    }
    return results;
  }
}
