package net.jet800.md5downloader;

import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;
import com.google.common.io.Files;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.net.URL;
import java.util.concurrent.Callable;

public class DownloadTask implements Callable<DownloadResult> {

  final URL source;
  final File localFile;
  final String expectedMD5;

  /**
   * Creates a DownloadTask which would download file from specified URL and compute it's MD5 hash.
   * @param source where to download file from
   * @param localFile where to save file locally
   * @param expectedMD5 expected MD5 sum of file
   */
  public DownloadTask(URL source, File localFile, String expectedMD5) {
    this.source = source;
    this.localFile = localFile;
    this.expectedMD5 = expectedMD5;
  }

  /**
   * Downloads file from specified URL and computes it's MD5 hash.
   *
   * @return computed result
   * @throws Exception if unable to compute a hash or download failed
   */
  public DownloadResult call() throws Exception {
    FileUtils.copyURLToFile(source, localFile, 10000, 10000);

    @SuppressWarnings("UnstableApiUsage")
    HashCode hash = Files.asByteSource(localFile).hash(Hashing.md5());

    return new DownloadResult(source, localFile, expectedMD5, hash.toString().toLowerCase());
  }
}
