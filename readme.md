# MD5 Downloader

Simple class that gets a comma-separated list of URLs and their respective checksums and then downloads given URLs and computes actual checksums.

Underlying implementation is based ExecutorService, commons-io and Guava. Files are downloaded into working directory in parallel, resuls are outputted in order of appearance in source file.

## Example usage

    java -jar md5-downloader.jar http://some.url/to.file
## License
Apache License, Version 2.0
http://www.apache.org/licenses/LICENSE-2.0